# Open Mobility indicator Jupyter notebook "jobs and population 350m around parkings"

## description : [indicator.yml file](https://gitlab.com/open-mobility-indicators/indicators/parkings/-/blob/main/indicator.yaml)

This indicator calculates the number of trips passing by each publich transport stop

forked from [jupyter notebook template](https://gitlab.com/open-mobility-indicators/indicator-templates/jupyter-notebook)

[Install locally using a venv or Docker, Build and Use (download, compute)](https://gitlab.com/open-mobility-indicators/indicator-templates/jupyter-notebook/-/blob/main/README.md#jupyter-notebook-indicator-template)  

